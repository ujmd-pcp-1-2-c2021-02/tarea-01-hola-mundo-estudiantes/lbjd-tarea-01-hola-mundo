﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LBJD_Tarea_01__Hola_Mundo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Bienvenido a mi mundo guerrero.";
            label1.BackColor = Color.DarkBlue;
            label1.ForeColor = Color.LightYellow;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "Nos vemos pronto guerrero.";
            label1.BackColor = Color.Khaki;
            label1.ForeColor = Color.DarkCyan;
        }
    }
}
